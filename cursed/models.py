import jwt

from datetime import datetime, timedelta

from django.conf import settings
from django.db import models
from django.core import validators
from django.contrib.auth.models import (
    AbstractBaseUser,
    PermissionsMixin,
    BaseUserManager,
)


class UserManager(BaseUserManager):
    """
    Django требует, чтобы пользовательские `User`
    определяли свой собственный класс Manager.
    Унаследовав от BaseUserManager, мы получаем много кода,
    используемого Django для создания `User`.

    Все, что нам нужно сделать, это переопределить функцию
    `create_user`, которую мы будем использовать
    для создания объектов `User`.
    """

    def _create_user(self, name, email, password=None, **extra_fields):
        if not name:
            raise ValueError("Указанное имя пользователя должно быть установлено")

        if not email:
            raise ValueError("Данный адрес электронной почты должен быть установлен")

        email = self.normalize_email(email)
        user = self.model(name=name, email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_user(self, name, email, password=None, **extra_fields):
        """
        Создает и возвращает `User` с адресом электронной почты,
        именем пользователя и паролем.
        """
        extra_fields.setdefault("isStaff", False)
        extra_fields.setdefault("is_superuser", False)

        return self._create_user(name, email, password, **extra_fields)

    def create_superuser(self, name, email, password, **extra_fields):
        """
        Создает и возвращает пользователя с правами
        суперпользователя (администратора).
        """
        extra_fields.setdefault("isStaff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("isStaff") is not True:
            raise ValueError("Суперпользователь должен иметь isStaff=True.")

        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Суперпользователь должен иметь is_superuser=True.")

        return self._create_user(name, email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    name = models.CharField(db_index=True, max_length=255)
    lastName = models.CharField(db_index=True, max_length=255)
    email = models.EmailField(
        validators=[validators.validate_email], unique=True, blank=False
    )
    currentSection = models.IntegerField(blank=True, default=1)
    isStaff = models.BooleanField(default=False)
    isActive = models.BooleanField(default=True)

    # Свойство `USERNAME_FIELD` сообщает нам, какое поле мы будем использовать для входа.
    USERNAME_FIELD = "email"

    REQUIRED_FIELDS = ("name",)

    # Сообщает Django, что класс UserManager, определенный выше,
    # должен управлять объектами этого типа.
    objects = UserManager()

    def __str__(self):
        """
        Возвращает строковое представление этого `User`.
        Эта строка используется, когда в консоли выводится `User`.
        """
        return self.name

    @property
    def token(self):
        """
        Позволяет нам получить токен пользователя, вызвав `user.token` вместо
        `user.generate_jwt_token().

        Декоратор `@property` выше делает это возможным.
        `token` называется «динамическим свойством ».
        """
        return self._generate_jwt_token()

    def get_full_name(self):
        """
        Этот метод требуется Django для таких вещей,
        как обработка электронной почты.
        Обычно это имя и фамилия пользователя.
        Поскольку мы не храним настоящее имя пользователя,
        мы возвращаем его имя пользователя.
        """
        return self.name

    def get_short_name(self):
        """
        Этот метод требуется Django для таких вещей,
        как обработка электронной почты.
        Как правило, это будет имя пользователя.
        Поскольку мы не храним настоящее имя пользователя,
        мы возвращаем его имя пользователя.
        """
        return self.name

    def _generate_jwt_token(self):
        """
        Создает веб-токен JSON, в котором хранится идентификатор
        этого пользователя и срок его действия
        составляет 60 дней в будущем.
        """
        dt = datetime.now() + timedelta(days=60)

        token = jwt.encode(
            {"id": self.pk, "exp": int(dt.strftime("%S"))},
            settings.SECRET_KEY,
            algorithm="HS256",
        )

        return token.decode("utf-8")

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"


class Sections(models.Model):
    orderNumber = models.IntegerField(unique=True)
    name = models.CharField("Название секции", max_length=100)
    description = models.TextField("Описание секции", null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Секция"
        verbose_name_plural = "Секции"


class Tests(models.Model):
    name = models.CharField(max_length=100)
    section = models.OneToOneField(Sections, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Тест"
        verbose_name_plural = "Тесты"


class Lectures(models.Model):
    section = models.ForeignKey(Sections, on_delete=models.CASCADE)
    content = models.TextField("Текст лекции")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Лекция"
        verbose_name_plural = "Лекции"


class Questions(models.Model):
    content = models.TextField()
    test = models.ForeignKey(Tests, on_delete=models.CASCADE, related_name="questions")

    def __str__(self):
        return self.content

    class Meta:
        verbose_name = "Вопрос"
        verbose_name_plural = "Вопросы"


class Answers(models.Model):
    content = models.TextField("Вопроса")
    question = models.ForeignKey(Questions, on_delete=models.CASCADE, related_name="answers")
    isRight = models.BooleanField("Правильность ответа", blank=True, default=False)

    def __str__(self):
        return self.content

    class Meta:
        verbose_name = "Ответ"
        verbose_name_plural = "Ответы"


class userResults(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    section = models.ForeignKey(Sections, on_delete=models.CASCADE, default = 1)
    test = models.ForeignKey(Tests, on_delete=models.CASCADE)
    correctAnswers = models.IntegerField(null=True, blank=True)
    totalAnswers = models.IntegerField(null=True, blank=True)
    lastCorrectPercent = models.FloatField(null=True, blank=True)
    maxCorrectPercent = models.FloatField(null=True, blank=True, default = 0)

    class Meta:
        verbose_name = "Результат теста"
        verbose_name_plural = "Результаты тестов"


class User_Answers(models.Model):
    answer = models.ForeignKey(Answers, on_delete=models.CASCADE)
    question = models.ForeignKey(Questions, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    isRight = models.BooleanField("Правильность ответа", blank=True, default=False)
    userResult = models.ForeignKey(userResults, on_delete=models.CASCADE, related_name='userAnswers', blank=True)

    class Meta:
        verbose_name = "Ответ пользователя"
        verbose_name_plural = "Ответы пользователей"


class Images(models.Model):
    image = models.ImageField(upload_to = 'images')

    class Meta:
        verbose_name = "Изображение в лекции"
        verbose_name_plural = "Изображения в лекциях"