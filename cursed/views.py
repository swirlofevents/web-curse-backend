from django.shortcuts import render, get_object_or_404
from rest_framework import status, viewsets, permissions
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from manager_utils import bulk_upsert
import json
from django.core.exceptions import ObjectDoesNotExist

from .urls import *
from .models import User, Tests, Sections, Lectures
from .serializers import *

permission = (permissions.AllowAny,)

schema_view = get_schema_view(
    openapi.Info(
        title="REST API",
        default_version="v1",
        description="Test description",
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)


class UserResultsView(viewsets.ReadOnlyModelViewSet):
    """Вывод результатов тестов, которые прошли пользователи"""

    permission_classes = permission
    queryset = userResults.objects.all().order_by("section")
    serializer_class = UserResultsSerializer

    def retrieve(self, request, pk):
        content = []
        results = self.queryset.filter(user=pk)
        for result in results:
            data = UserResultsRetriveSerializer(result).data
            data['section'] = Sections.objects.get(pk=data['section'] ).orderNumber
            data['test'] = Sections.objects.get(orderNumber=data['section'] ).name
            for i in range(0, len(data.get("userAnswers"))):
                data["userAnswers"][i]["question"] = Questions.objects.get(
                    pk = data["userAnswers"][i]["question"]
                ).content
                data["userAnswers"][i]["answer"] = Answers.objects.get(
                    pk = data["userAnswers"][i]["answer"]
                ).content
            content.append(data)
        return Response(content)


class UsersView(viewsets.ModelViewSet):
    """Вывод списка пользователей"""

    permission_classes = permission
    queryset = User.objects.all()
    serializer_class = UsersSerializer

    def retrieve(self, request, pk):
        user = get_object_or_404(self.queryset, pk=pk)
        section = Sections.objects.get(orderNumber=user.currentSection)
        content = {
            **self.serializer_class(user).data,
            "section": SectionsSerializer(section).data,
        }
        return Response(content)


class TestsView(viewsets.ModelViewSet):
    """Вывод тестов"""

    permission_classes = permission
    queryset = Tests.objects.all()
    serializer_class = TestsSerializer

    def retrieve(self, request, pk):
        getSection = get_object_or_404(Sections.objects.all(), orderNumber=pk)
        test = get_object_or_404(self.queryset, section=getSection.pk)
        serializer = self.serializer_class(test)
        return Response(serializer.data)


class SectionsView(viewsets.ModelViewSet):
    """Вывод секций"""

    permission_classes = permission
    queryset = Sections.objects.all().order_by("orderNumber")
    serializer_class = SectionsSerializer

    def retrieve(self, request, pk):
        section = get_object_or_404(self.queryset, orderNumber=pk)
        lecture = Lectures.objects.filter(section=section.pk).first()
        content = {
            **self.serializer_class(section).data,
            "lecture": LecturesSerializer(lecture).data,
        }
        return Response(content)


class LecturesView(viewsets.ModelViewSet):
    """Вывод лекций"""

    permission_classes = permission
    queryset = Lectures.objects.all()
    serializer_class = LecturesSerializer


class GetUserInfoView(APIView):
    """Получение информации о пользователе по токену"""

    permission_classes = (permissions.AllowAny,)
    serializer_class = GetUserInfo

    @swagger_auto_schema(
        request_body=GetUserInfo,
        responses={
            "200": "OK",
            "400": "Bad Request",
        },
        security=[],
        operation_id="Getting User Info",
        operation_description="Getting user information by token",
    )
    def post(self, request):
        token = self.serializer_class(data=request.data)
        token.is_valid(raise_exception=True)
        dt = jwt.decode(
            request.data["token"],
            settings.SECRET_KEY,
            algorithms=["HS256"],
            options={"verify_exp": False},
        )
        user = User.objects.get(pk=dt["id"])
        
        try:
            section = Sections.objects.get(orderNumber=user.currentSection)
            content = {
            "id": user.pk,
            "name": user.name,
            "lastName": user.lastName,
            "email": user.email,
            "currentSection": SectionsSerializer(section).data,
            }
            return Response(content)

        except ObjectDoesNotExist:

            content = {
            "id": user.pk,
            "name": user.name,
            "lastName": user.lastName,
            "email": user.email,
            "currentSection": SectionsSerializer(
                get_object_or_404(
                    Sections.objects.all(), orderNumber=user.currentSection - 1
                )
            ).data,
            }
            return Response(content)


class ImagesView(viewsets.ModelViewSet):
    """Вьюха изображений"""

    permission_classes = permission
    queryset = Images.objects.all()
    serializer_class = ImagesSerializer


class RegistrationAPIView(APIView):
    """
    Registers a new user.
    """

    permission_classes = (permissions.AllowAny,)
    serializer_class = RegistrationSerializer

    @swagger_auto_schema(
        request_body=RegistrationSerializer,
        responses={"200": "OK", "400": "Bad Request"},
        security=[],
        operation_id="Register",
        operation_description="Create new user and return token",
    )
    def post(self, request):
        """
        Creates a new User object.
        Name, last name, email, and password are required.
        Returns a JSON web token.
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            {
                "token": serializer.data.get("token", None),
            },
            status=status.HTTP_201_CREATED,
        )


class DataChangeAPIView(APIView):
    """
    Data change.
    """

    permission_classes = (permissions.AllowAny,)
    serializer_class = DataChangeSerializer

    @swagger_auto_schema(
        request_body=DataChangeSerializer,
        responses={"200": "OK", "400": "Bad Request"},
        security=[],
        operation_id="Data change",
        operation_description="Data change by email and old password and return token",
    )
    def post(self, request):
        """
        Checks is user exists.
        Email and password are required.
        Returns a JSON web token.
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(serializer.data, status=status.HTTP_200_OK)


class LoginAPIView(APIView):
    """
    Logs in an existing user.
    """

    permission_classes = (permissions.AllowAny,)
    serializer_class = LoginSerializer

    @swagger_auto_schema(
        request_body=LoginSerializer,
        responses={"200": "OK", "400": "Bad Request"},
        security=[],
        operation_id="Login",
        operation_description="Login by credentials and return token",
    )
    def post(self, request):
        """
        Checks is user exists.
        Email and password are required.
        Returns a JSON web token.
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(serializer.data, status=status.HTTP_200_OK)


class UserAnswersView(viewsets.ModelViewSet):
    """User Answers View"""

    permission_classes = permission
    queryset = User_Answers.objects.all()
    serializer_class = UserAnswersSerializer

    def create(self, request):
        """Create Users Answers"""

        """Объекты"""
        user = User.objects.get(pk=request.data.get("userId"))

        if request.data["sectionId"] != user.currentSection:
            return Response("Неверный номер секции", status=status.HTTP_400_BAD_REQUEST)
        test = Tests.objects.get(pk=request.data.get("testId"))
        section = Sections.objects.get(orderNumber = request.data.get("sectionId"))

        """Переменные"""
        answersObjects = []
        answers = request.data.pop("answers")
        totalAnswers = len(answers)
        correctAnswers = 0

        maxResults = userResults.objects.filter(user=user.pk, test=test.pk).first()
        if maxResults:
            maxCorrectPercent = maxResults.maxCorrectPercent
        else:
            maxCorrectPercent = 0

        try:
            createdResult = userResults.objects.get(user=user.pk, test=test.pk, section=section.pk)
        except ObjectDoesNotExist:

            """Создание "пустого" результата для корректного создания ответов"""

            userTestResults = {
                "user": user.pk,
                "test": test.pk,
                "section": section.pk, 
                "correctAnswers": 0,
                "totalAnswers": 0,
                "lastCorrectPercent": 0,
                "maxCorrectPercent": maxCorrectPercent,
            }
            resultsSerializer = UserResultsSerializer(data=userTestResults)
            resultsSerializer.is_valid(raise_exception=True)

            obj = userResults(
                user=user,
                test=test,
                section=section,
                correctAnswers=0,
                totalAnswers=0,
                lastCorrectPercent=0,
                maxCorrectPercent=maxCorrectPercent,
            )
            objects = [obj]

            bulk_upsert(
                userResults.objects.all(),
                objects,
                ["user", "test", "section"],
                [
                    "correctAnswers",
                    "totalAnswers",
                    "lastCorrectPercent",
                    "maxCorrectPercent",
                ]
            )
            createdResult = userResults.objects.get(user=user.pk, test=test.pk)

        """Цикл для обработкаи всех ответо пользователей"""

        for elem in answers:

            answer = Answers.objects.get(pk=elem.get("answer"))
            question = Questions.objects.get(pk=elem.get("question"))
            userAnswer = {
                "user": user.pk,
                "question": question.pk,
                "answer": answer.pk,
                "isRight": answer.isRight,
            }

            if answer.isRight:
                correctAnswers += 1

            """Валидация ответо пользователей"""

            answerSerializer = self.serializer_class(data=userAnswer)
            answerSerializer.is_valid(raise_exception=True)
            obj = User_Answers(
                user=user,
                question=question,
                answer=answer,
                isRight=answer.isRight,
                userResult=createdResult,
            )
            answersObjects.append(obj)

        """Добавление в БД ответов пользователей"""

        bulk_upsert(
            User_Answers.objects.all(),
            answersObjects,
            ["user", "question"],
            ["answer", "isRight", "userResult"],
        )

        """Create Users Results"""

        correctPercent = (correctAnswers / totalAnswers) * 100

        if correctPercent > maxCorrectPercent:
            maxCorrectPercent = correctPercent

        userTestResults = {
            "user": user.pk,
            "test": test.pk,
            "section": section.pk, 
            "correctAnswers": correctAnswers,
            "totalAnswers": totalAnswers,
            "lastCorrectPercent": correctPercent,
            "maxCorrectPercent": maxCorrectPercent,
        }
        resultsSerializer = UserResultsSerializer(data=userTestResults)
        resultsSerializer.is_valid(raise_exception=True)

        obj = userResults(
            user=user,
            test=test,
            section=section,
            correctAnswers=correctAnswers,
            totalAnswers=totalAnswers,
            lastCorrectPercent=correctPercent,
            maxCorrectPercent=maxCorrectPercent,
        )
        objects = [obj]

        """Добавление результатов тестов пользователей"""

        bulk_upsert(
            userResults.objects.all(),
            objects,
            [
                "user",
                "test",
                "section",
            ],
            [
                "correctAnswers",
                "totalAnswers",
                "lastCorrectPercent",
                "maxCorrectPercent",
            ],
        )

        user.currentSection = user.currentSection + 1
        user.save()

        return Response(resultsSerializer.data, status=status.HTTP_201_CREATED)


class GetResultForSectionAPI(APIView):
    """Получение результатов теста секции по юзеру и секции"""

    permission_classes = (permissions.AllowAny,)
    serializer_class = GetResultForSection

    @swagger_auto_schema(
        request_body=GetResultForSection,
        responses={
            "200": "OK",
            "404": "Not found",
        },
        security=[],
        operation_id="Getting result for section",
        operation_description="Getting result information by user, section order number",
    )
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            result = userResults.objects.get(user=serializer.data.get("userId"), section=Sections.objects.get(orderNumber = serializer.data.get("sectionId")).pk)
            resultSer = UserResultsSerializer(instance=result)

        except ObjectDoesNotExist:
            return Response({
                "Error": "объекта не существует",
            }, status=status.HTTP_404_NOT_FOUND)

        return Response(resultSer.data, status=status.HTTP_200_OK)
        