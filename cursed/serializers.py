from rest_framework import serializers
from .models import *
from django.contrib.auth import authenticate
import jwt
from django.core.exceptions import ObjectDoesNotExist

class UsersSerializer(serializers.ModelSerializer):
    """Список пользователей"""

    class Meta:
        model = User
        fields = ("id", "name", "lastName", "email", "currentSection")


class RegistrationSerializer(serializers.ModelSerializer):
    """
    Creates a new user.
    Email, name, and password are required.
    Returns a JSON web token.
    """

    # The password must be validated and should not be read by the client
    password = serializers.CharField(
        max_length=128,
        min_length=8,
        write_only=True,
    )

    # The client should not be able to send a token along with a registration
    # request. Making `token` read-only handles that for us.
    token = serializers.CharField(max_length=255, read_only=True)

    class Meta:
        model = User
        fields = (
            "email",
            "name",
            "lastName",
            "password",
            "token",
        )

    def create(self, validated_data):
        validated_data['name'] = validated_data['name'].capitalize()
        validated_data['lastName'] = validated_data['lastName'].capitalize()
        
        section = Sections.objects.all().order_by('orderNumber').first()
        if section is None:
            validated_data['currentSection'] = 1
        else:
            validated_data['currentSection'] = section.orderNumber
        return User.objects.create_user(**validated_data)


class LoginSerializer(serializers.Serializer):
    """
    Authenticates an existing user.
    Email and password are required.
    Returns a JSON web token.
    """

    email = serializers.EmailField(write_only=True)
    password = serializers.CharField(max_length=128, write_only=True)

    # Ignore these fields if they are included in the request.
    token = serializers.CharField(max_length=255, read_only=True)

    def validate(self, data):
        """
        Validates user data.
        """
        email = data.get("email", None)
        password = data.get("password", None)

        if email is None:
            raise serializers.ValidationError("An email address is required to log in.")

        if password is None:
            raise serializers.ValidationError("A password is required to log in.")

        user = authenticate(username=email, password=password)

        if user is None:
            raise serializers.ValidationError(
                "A user with this email and password was not found."
            )

        if not user.isActive:
            raise serializers.ValidationError("This user has been deactivated.")

        return {
            "token": user.token,
        }


class DataChangeSerializer(serializers.Serializer):
    """
    Data change an existing user.
    Email and password are required.
    Returns a JSON web token.
    """

    oldEmail = serializers.EmailField(write_only=True)
    newEmail = serializers.EmailField(write_only=True, default=None)
    oldName = serializers.CharField(max_length=255, write_only=True, default=None)
    newName = serializers.CharField(max_length=255, write_only=True, default=None)
    oldLastName = serializers.CharField(max_length=255, write_only=True, default=None)
    newLastName = serializers.CharField(max_length=255, write_only=True, default=None)
    oldPassword = serializers.CharField(max_length=128, write_only=True)
    newPassword = serializers.CharField(max_length=128, write_only=True, default=None)

    token = serializers.CharField(max_length=255, read_only=True)

    def validate(self, data):
        """
        Validates user data.
        """

        oldEmail = data.get("oldEmail", None)
        newEmail = data.get("newEmail", None)
        oldName = data.get("oldName", None)
        newName = data.get("newName", None)
        oldLastName = data.get("oldLastName", None)
        newLastName = data.get("newLastName", None)
        oldPassword = data.get("oldPassword", None)
        newPassword = data.get("newPassword", None)

        if oldEmail is None:
            raise serializers.ValidationError("An email address is required to change data.")

        if oldPassword is None:
            raise serializers.ValidationError("A password is required to change data.")

        user = authenticate(username=oldEmail, password=oldPassword)

        if user is None:
            raise serializers.ValidationError(
                "A user with this email and password was not found."
            )

        if not user.isActive:
            raise serializers.ValidationError("This user has been deactivated.")

        if newPassword is not None:
            if newPassword != oldPassword:
                user.set_password(newPassword)
            else:
                raise serializers.ValidationError("New password == old password")

        if newEmail is not None:
            if newEmail != oldEmail:
                user.email = newEmail.capitalize()
            else:
                raise serializers.ValidationError("New email == old email")
            
        if newName is not None:
            if newName != oldName:
                 user.name = newName.capitalize()
            else:
                raise serializers.ValidationError("New name == old name")
            
        if newLastName is not None:
            if newLastName != oldLastName:
                user.lastName = newLastName.capitalize()
            else:
                raise serializers.ValidationError("New lastName == old lastName")
            
        user.save()
        
        return {
            "token": user.token,
        }


class GetUserInfo(serializers.Serializer):
    """
    Получение информации
    о пользователе
    используя токен
    """

    token = serializers.CharField(max_length=255)

    def validate(self, data):
        token = data.get("token", None)
        if token is None:
            raise serializers.ValidationError("A token is required.")
        return token


class AnswersSerializer(serializers.ModelSerializer):
    """Сериализатор ответов"""

    class Meta:
        model = Answers
        fields = (
            "id",
            "content",
            "isRight",
        )


class QuestionsSerializer(serializers.ModelSerializer):
    """Список вопросов"""

    answers = AnswersSerializer(many=True)

    class Meta:
        model = Questions
        fields = ("id","content", "answers")


class TestsSerializer(serializers.ModelSerializer):
    """Список тестов"""

    questions = QuestionsSerializer(many = True)  

    class Meta:
        model = Tests
        fields = ("id","section", "name", "questions")

    def create(self, validated_data):
        questions_data = validated_data.pop('questions')
        validated_data['section'] = Sections.objects.get(orderNumber=validated_data.get('section').pk)
        test = Tests.objects.create(**validated_data)

        for question_data in questions_data:
            answers_data = question_data.pop('answers')
            question = Questions.objects.create(test=test, **question_data)

            for answer_data in answers_data:
                Answers.objects.create(question=question, **answer_data)
        return test

    def update(self, instance, validated_data):
        questions_data = validated_data.pop('questions')
        questions = (instance.questions).all()
        questions = list(questions)
        instance.name = validated_data.get('name', instance.name)
        instance.section = validated_data.get('section', instance.section)
        instance.save()

        for question_data in questions_data:
            question = questions.pop(0)
            answers_data = question_data.pop('answers')
            answers = (question.answers).all()
            answers = list(answers)
            question.content = question_data.get('content', question.content)
            question.save()
            
            for answer_data in answers_data:
                answer = answers.pop(0)
                answer.content = answer_data.get('content', answer.content)
                answer.isRight = answer_data.get('isRight', answer.isRight)
                answer.save()
        
        return instance


class SectionsSerializer(serializers.ModelSerializer):
    """Сериализатор секций"""

    class Meta:
        model = Sections
        fields = (
            "id",
            "orderNumber",
            "name",
            "description",
        )


class LecturesSerializer(serializers.ModelSerializer):
    """Сериализатор лекций"""

    class Meta:
        model = Lectures
        fields = (
            "id",
            "section",
            "content",
        )


class QuestionsSerializer(serializers.ModelSerializer):
    """Список вопросов"""

    class Meta:
        model = Questions


class ImagesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Images
        fields = "__all__"


class UserAnswersSerializer(serializers.ModelSerializer):
    """Ответы пользователей"""

    class Meta:
        model = User_Answers
        fields = ('user', 'question', 'answer', 'isRight')


class UserResultsSerializer(serializers.ModelSerializer):
    """Резльутаты пользователей"""
    
    class Meta:
        model = userResults
        fields = ('user', 'test', 'section', 'correctAnswers', 'totalAnswers', 'lastCorrectPercent', 'maxCorrectPercent')


class UserAnswersSerializerForResults(serializers.ModelSerializer):
    """Ответы пользователей для вывода результатов"""

    class Meta:
        model = User_Answers
        fields = ('question', 'answer', 'isRight')

    
class UserResultsRetriveSerializer(serializers.ModelSerializer):
    """Резльутаты пользователей"""

    userAnswers = UserAnswersSerializerForResults(many = True)
    class Meta:
        model = userResults
        fields = ('user', 'test', 'section', 'correctAnswers', 'totalAnswers', 'lastCorrectPercent', 'maxCorrectPercent', 'userAnswers')


class GetResultForSection(serializers.Serializer):
    """
    Получение результатов
    теста секции
    используя пользователя
    """

    userId = serializers.IntegerField()
    sectionId = serializers.IntegerField()

    def validate(self, data):
        user = data.get("userId", None)
        section = data.get("sectionId", None)
        if user is None:
            raise serializers.ValidationError("A user is required.")
        if section is None:
            raise serializers.ValidationError("A section is required.")

        return {
            "userId": user,
            "sectionId": section,
        }