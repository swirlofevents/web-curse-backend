from django.urls import path, re_path
from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from .views import *

router = DefaultRouter()
router.register(r"test", TestsView, basename="test")
router.register(r"users", UsersView, basename="user")
router.register(r"section", SectionsView, basename="section")
router.register(r"lecture", LecturesView, basename="lecture")
router.register(r"image", ImagesView, basename="image")
router.register(r"userAnswers", UserAnswersView, basename="user answers")
router.register(r"userResults", UserResultsView, basename='user results')


urlpatterns = [
    url(r"^", include(router.urls)),
    url(r'^doc(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^doc/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    re_path(r"^userinfo/?$", GetUserInfoView.as_view(), name="user_information"),
    re_path(
        r"^registration/?$", RegistrationAPIView.as_view(), name="user_registration"
    ),
    re_path(r"^login/?$", LoginAPIView.as_view(), name="user_login"),
    re_path(r"dataChange/?$", DataChangeAPIView.as_view(), name="Data_change"),
    re_path(r"resultSection/?$", GetResultForSectionAPI.as_view(), name="Section_result")
]